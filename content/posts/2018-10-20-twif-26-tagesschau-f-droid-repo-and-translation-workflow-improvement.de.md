---
layout: post
title: "TWIF 26: F-Droid-Repo der Tagesschau und Verbesserung des Übersetzungsworkflow"
lang: de
edition: 26
author: "Coffee"
authorWebsite: "https://open.source.coffee"
fdroid: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #0d47a1; font-style: normal; font-weight: bold;">F-Droid</em>'
featuredv1: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.5ex; box-shadow: 0.1em 0.05em 0.1em rgba(0, 0, 0, 0.3); border-radius: 1em; color: black; background: linear-gradient(orange, yellow);">Featured</em>'
featured: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: orange; font-style: normal; font-weight: bold;">Featured</em>'
major: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #8ab000; font-style: normal; font-weight: bold;">Major</em>'
number_of_updated_apps: 79
mastodonAccount: "**[@fdroidorg@mastodon.technology](https://mastodon.technology/@fdroidorg)**"
twifTag: "**[#TWIF](https://mastodon.technology/tags/twif)**"
twifThread: "[TWIF submission thread](https://forum.f-droid.org/t/twif-submission-thread)"
matrixRoom: "[#fdroid:matrix.org](https://matrix.to/#/#fdroid:matrix.org)"
telegramRoom: "https://t.me/joinchat/AlRQekvjWDTuQrCgMYSNVA"
forum: "https://forum.f-droid.org"
---


Diese Woche In F-Droid {{ page.edition }}, Woche {{ page.date | date: "%V, %G" }} <a href="{{ site.baseurl }}/feed.xml"><img src="{{ site.baseurl }}/assets/Feed-icon-16x16.png" alt="Feed"></a>

In dieser Ausgabe: Die Tagesschau betreibt ein F-Droid-Repo, Beginn der Übersetzungsoptimierung, Abschluss der Buildserver-Automation, sowie 2 neue und {{ page.number_of_updated_apps }} aktualisierte Apps.
<!--more-->

[F-Droid](https://f-droid.org/) ist ein [Repository](https://f-droid.org/de/packages/) verifizierter [Free & Open Source](https://de.wikipedia.org/wiki/Free/Libre_Open_Source_Software) Android Apps, ein [Client](https://f-droid.org/app/org.fdroid.fdroid), um darauf zuzugreifen, sowie ein ganzes "App Store Set", das alle notwendigen Werkzeuge zum Einrichten und Betreiben eines App Stores bietet. Es ist ein gemeinschaftlich geführtes freies Softwareprojekt, das von einer Vielzahl von Mitwirkenden weiterentwickelt wird. Das ist ihre Geschichte der letzten Woche.

#### Die Tagesschau betreibt ihr eigenes F-Droid-Repository

Wir haben kürzlich erfahren, dass die
[Tagesschau](https://en.wikipedia.org/wiki/Tagesschau_(German_TV_series))
ihre offizielle App über ihr eigenes F-Droid-Repository auf
`https://service.tagesschau.de/app/repo` vertreibt.

Wir betrachten diesen Zug als großen Schritt in Richtung Anwenderrechte und
-freiheiten. Bedauerlicherweise ist die App keine [Free and Open Source
Software](https://en.wikipedia.org/wiki/Free_and_open-source_software),
obwohl sie mit öffentlichen Geldern finanziert ist. Wenn es öffentliche
Gelder sind, sollte es auch [öffentlicher
Programmcode](https://publiccode.eu) sein.

Hintergrund für nicht-deutsche Leser: Die Tagesschau ist die zentrale,
deutsche TV-Nachrichtensendung, die jeden Tag um 20:00 Uhr ausgestrahlt
wird. Eine längere und nuanciertere Beschreibung finden Sie [in der
Wikipedia](https://de.wikipedia.org/wiki/Tagesschau_(ARD)).

#### Übersetzungsverbesserungen rund um Weblate laufen an

**[@\_hc](https://forum.f-droid.org/u/hans)** und **[@uniq](https://chaos.social/@uniq)** werden zusammenarbeiten, um im Rahmen eines geförderten Projekts den Weblate-Workflow zu verbessern. Die Arbeiten sind speziell auf Markdown ausgerichtet, wie wir es auf dieser Internetseite verwenden. Es werden auch Anstrengungen in Richtung App-Store-Übersetzungen wie [Fastlane](https://fastlane.tools) und [Triple-T](https://github.com/Triple-T) gehen. Zusätzlich bleibt ein wenig Zeit, um an der Stabilisierung von [Jekyll](https://jekyllrb.com) zu arbeiten, damit mehr als 9 Sprachen auf unserer Website unterstützt werden können. Sollten Sie Ruby-Entwickler sein, der bei Letzterem helfen möchte, nehmen Sie bitte [Kontakt](https://f-droid.org/en/about/#contact) mit uns auf.

#### Buildserver-Automatisierung

**[@uniq](https://chaos.social/@uniq)** hat seine grandiose Buildserver-Automatisierung mit einiger Hilfe von **[@\_hc](https://forum.f-droid.org/u/hans)** abgeschlossen. Dasselbe [Ansible](https://en.wikipedia.org/wiki/Ansible_%28software%29)-Setup kann auch für [Verifikationsserver](https://reproducible-builds.org) verwendet werden.

#### Neue Apps

* **[Adaptive Brightness
  Tile](https://f-droid.org/app/com.rascarlo.adaptive.brightness.tile)**:
  Schnelleinstellungskachel für adaptive Helligkeit.
* **[BookmarkOS](https://f-droid.org/app/com.weskenyon.bookmarkos)**: Diese
  Anwendung ermöglicht Ihnen die direkte URL-Freigabe aus dem
  Android-Browser geradewegs in BookmarkOS.

#### Aktualisierte Apps

Insgesamt wurden diese Woche **{{ page.number_of_updated_apps }}** Apps
aktualisiert. Hier die Höhepunkte:

* {{ page.featured }} Wussten Sie, dass Apps heutzutage verfolgen können, wo
  Sie sind und was Sie sich ansehen, indem sie (für Menschen) nicht hörbare,
  hochfrequente Geräusche nutzen? Durch Abhören Ihres Telefonmikrofons,
  filtern Apps, die bestimmte Tracker-Software enthalten, Töne von
  Fernsehern, Radios und Webseiten-Werbungen aber auch von Werbetafeln,
  Outlets und Sportarenen heraus. Sehen Sie sich als Beispiel [diesen
  SC-Magazinartikel](https://www.scmagazineuk.com/invasive-apps-track-ultrasonic-sounds-via-mobile-microphone/article/1474717)
  an.

  Kommen Sie zu **[PilferShush Jammer](https://f-droid.org/app/cityfreqs.com.pilfershushjammer)**, sowohl ein passiver als auch ein aktiver Mikrofonstörsender. Seine primäre Arbeitsweise ist, das Mikrofon ausschließlich geschlossen zu lassen, andere Apps davon abzuhalten es überhaupt zu öffnen. Wenn das nicht funktioniert, oder wenn Sie den Verdacht haben, System-Apps seien im Spiel, hat er auch einen aktiven Störsendermodus, der seine eigenen Töne über den Telefonlautsprecher aussendet, was hoffentlich die Geräusche für das Tracking überlagert. Augenscheinlich ist das weniger zuverlässig und natürlich ist es allemal besser, feindliche Closed Source Apps erst gar nicht zu installieren.

  So wurde diese Woche PilferShush Jammer von 2.0.13 auf 2.2.2 aktualisiert, und er kann jetzt andere Apps durchsuchen, ihre Fähikeiten auflisten und anzeigen, welche davon das NUHF Beacon SDK enthalten. Er kann auch eine einzelne App scannen und anzeigen, ob sie auf das Mikrofon zugreift, neben weiteren Dingen. Schließlich gibt es einen neuen Benachrichtigungskanal sowie weitere kleine Verbesserungen und Fehlerkorrekturen.

* **[Yalp Store](https://f-droid.org/app/com.github.yeriomin.yalpstore)**
  wurde von 0.43 auf 0.45-legacy aktualisiert, wobei die Unterstützung
  mehrerer Konten und die saubere Verarbeitung von APK-Splits sowie eine
  Menge Bugfixes und Verbesserungen hinzugefügt wurde.

* **[PocketMaps](https://f-droid.org/app/com.junjunguo.pocketmaps)** ist ein
  Kartenbetrachter mit Navigationssystem. Er wurde von 2.3 auf 2.6
  aktualisiert, zeigt nun den Entpackvorgang in der Statusleiste an und
  besitzt eine neue Auswahloption zur Angabe der Karte bei jedem Start,
  einen Hilfeeintrag im Hauptmenü, der auf die Online-Dokumentation
  weiterleitet, und verschiedene Lösungen für Absturzursachen.

* **[MatLog Libre](https://f-droid.org/app/com.pluscubed.matloglibre)**
  zeigt eine scrollende ("tail-te") Ansicht des Android "logcat"
  Systemprotokolls, daher der alberne Name. In Version 1.2.0 wurde das
  Widget für Android 8+ repariert und eine gefilterte Tags-Einstellung, das
  Laden der Fortschrittsanzeige aus der Datei und das Kernel-Log im Shared
  Zip wurden hinzugefügt. Neben anderen kleinen Verbesserungen wurden auch
  mehrere Abstürze behoben.

* {{ page.major }} Die "Simple Mobile Tools" **[File
  Manager](https://f-droid.org/app/com.simplemobiletools.filemanager)** und
  **[Flashlight](https://f-droid.org/app/com.simplemobiletools.flashlight)**
  erhielten Updates auf 5.0.1 bzw. 5.0.0, bei der die Minimalanforderung an
  die Android-Version auf 5.0 (von 4.1) angehoben und der G+ Button durch
  Reddit ersetzt wird, neben einigen kleinen Reparaturen.

* {{page.major}} **[Open Link
  With](https://f-droid.org/app/com.tasomaniac.openwith.floss)** wurde von
  1.9-floss auf 2.3-floss aktualisiert. Mit dieser App können Sie einen Link
  in einer anderen nativen App öffnen, wenn Sie im Browser festsitzen. Es
  unterstützt nun eine Browser-Vorauswahl, so dass Sie zwischen allen
  Browsern (bisheriges Verhalten), einem einzigen Browser oder gar keinen
  Browservorschlägen auswählen können. Häufig angeforderte erweiterte
  Einstellungen wurden ebenfalls hinzugefügt und Sie können OLW nun selbst
  als Browser einstellen, so dass alle Link-Klicks automatisch durch OLW
  gehen, anstatt "Share Link" verwenden zu müssen.

* **[Password Store](https://f-droid.org/app/com.zeapo.pwdstore)** speichert
  Passwörter in einfachen Textdateien, die mit OpenPGP verschlüsselt
  sind. Dieses Update auf 1.3.1 enthält ein neues adaptives Icon, die
  Möglichkeit zur Installation auf einer externen
  [SD-Karte](https://de.wikipedia.org/wiki/SD-Karte), eine andere
  Sortierreihenfolge für Passwörter, die Anzeige von HOTP-Code, falls
  vorhanden, eine sichere Passworteingabe und andere Bugfixes und kleinere
  Verbesserungen.

* **[Conversations](https://f-droid.org/app/eu.siacs.conversations)** wurde
  von 2.3.2+fcr auf 2.3.4+fcr aktualisiert, Übersetzungen aktualisiert,
  Verbindungsprobleme mit JIDs vom Typ user@ip behoben und das Senden von
  OMEMO-verschlüsselten Dateien an ChatSecure iOS-Benutzer korrigiert.

* {{ page.featured }} Wieder riesige Aktualisierungen bei **[Mastalab](https://f-droid.org/app/fr.gouv.etalab.mastodon)**! Diese Woche wurde es von 1.15.2 auf 1.17.1 aktualisiert, wobei es um Folgendes ging:
  - **Volle Unterstützung für [PeerTube](https://en.wikipedia.org/wiki/PeerTube)**: Kommentieren/Folgen/Boost/Antworten/Antworten/Fav von Ihrem eigenen Mastodon-Konto!
  - Neues Layout für PeerTube-Instanzen
  - Neuer Videoplayer, um PeerTube mit allen Geräten kompatibel zu machen.
  - Komplette interne Überarbeitung von Live-Updates, jetzt mit Websockets mit dem wss-Protokoll für Streaming-URLs.
  - Möglichkeit, Live-Updates nur dann zuzulassen, wenn die App läuft.
  - Ein roter Rahmen wurde für Medien hinzugefügt, die beim Zusaamenstellen keine Beschreibung haben.
  - Mehrere kleinere Verbesserungen und Bugfixes

* **[Riot.im](https://f-droid.org/app/im.vector.alpha)** und
  **[miniVector](https://f-droid.org/app/com.lavadip.miniVector)** beide mit
  Updates von 0.8.15 auf 0.8.18. Riot ist ein
  [Matrix](https://matrix.org)-Client für den (Gruppen)-Chat und VoiP/Video,
  mit optionaler End-zu-End-Verschlüsselung. miniVector ist ein
  leichtgewichtiger Zweig von Riot, bei dem einige schwerwiegende
  Abhängigkeiten wegfallen und der weniger Berechtigungen benötigt, auf
  Kosten der VoiP/Video-Unterstützung. Änderungen schließen Verbesserungen
  der Dialoge, Videonachrichten und der Vorschauleistung ein; ein neues
  Status.im-Design; sowie die Unterstützung eines "lazy loading", einer
  riesigen Veränderung unter der Haube, die die Startzeit, den
  Speicherbedarf und den Datenverbrauch reduziert. Das letztgenannte Feature
  wird noch als instabil angesehen und muss über die "labs"-Einstellung
  aktiviert werden.

* **[Syncopoli](https://f-droid.org/app/org.amoradi.syncopoli)** ist ein
  rsync-Client für Ihr Telefon (derzeit nur in der Richtung vom Telefon zur
  Fernbedienung). Version v0.5 bietet die Möglichkeit, mehrere
  Quellverzeichnisse hinzuzufügen, die Möglichkeit, den Fingerabdruck des
  ssh-Hosts zu überprüfen, die richtige Handhabung von Speicherplatz und
  Zeichenketten in den rsync-Optionen, die globale Konfiguration der
  exportierten Konfigurationsdatei sowie verschiedene Fehlerbehebungen.

* **[Blokada v3 (ad blocker)](https://f-droid.org/app/org.blokada.alarm)**
  wurde von 3.4.100500 auf 3.6.101401 aktualisiert, mit allgemein
  verbesserter Tunnelstabilität, vermindertem Datenverbrauch, neuem
  Bildschirm für erweiterte Einstellungen, einer Einstellung, um zu
  verhindern, dass das System im Akkusparbetrieb Blokada beendet, einer
  Einstellung, um Listen nur über WLAN zu aktualisieren, weiteren kleinen
  Weiterentwicklungen und zahlreichen Bugfixes.

* **[The Light](https://f-droid.org/app/org.hlwd.bible)** liefert die Bibel
  in mehreren Sprachen. Version 3.4 ist eine kleine Instandhaltungsversion,
  die 13 Artikel und 7 Youtuber hinzufügt.

* **[Emerald Launcher](https://f-droid.org/app/ru.henridellal.emerald)** ist
  ein leichtgewichtiger, anpassbarer Launcher, der Symbolpakete unterstützt
  und für seine Größe eine ansehnliche Zahl von Einstellungen bietet. Diese
  Woche erhielt er ein Update von 0.5.6.2 nach 0.6.0.2, nun mit
  Schnellverknüpfungen, dem Vollbildmodus, der Option "Tastatur beim Start
  einblenden", Kategorien mit den Lautstärketasten wechseln, einem
  verbesserten Tutorial-Design, Übernahme eines benutzerdefinierten Icons
  aus dem Speicher und diversen Korrekturen.

* **[Manyverse](https://f-droid.org/app/se.manyver)** wurde von
  0.18.10-02.beta auf 0.1810.16-beta aktualisiert, lässt Sie nun Ihr
  Profilbild bearbeiten und erledigt die initiale Synchronisation 5-mal
  schneller und besser.

* **[AnySoftKeyboard](https://f-droid.org/app/com.menny.android.anysoftkeyboard)**-Update von 1.9.2055 auf 1.9.2445 bringt:
  - Verbesserungen für die Akkuoptimierung - Sie können übernehmen, welche Funktionen in die Optimierung aufgenommen werden sollen.
  - Daneben ermöglichen wir ein Umschalten zu einem dunklen, einfachen Design im Energiesparmodus. Aber das ist optional.
  - Neues Workman Layout, Terminal generic-top-row and long-press Fixes. Von Alex Griffin umgesetzt.
  - Aktualisierte Sprachen: AR, BE, EU, FR, HU, IT, KA, KN, KU, LT, NB, NL, PT, RO, RU, SC, UK.
  - Lösung für geringere Textvorschläge auf manchen Geräten

#### Beta-Versionen

Die folgende App wurde ebenfalls aktualisert, wird aber nicht als
aktualisierbar angezeigt, solange Sie nicht die Option "Instabile
Aktualisierungen" in den F-Droid-Einstellungen gewählt haben:

* **[ownCloud](https://f-droid.org/app/com.owncloud.android)** wurde von
  2.9.0-beta.1 auf 2.9.0-beta.2 aktualisiert

#### Anregungen und Rückmeldung

Haben Sie wichtige Updates einer App, über die wir schreiben sollten? Senden
Sie Ihre Tipps über [Mastodon](https://joinmastodon.org) ein! Senden Sie sie
an {{ page.mastodonAccount }} und denken Sie an einen {{ page.twifTag
}}-Tag. Oder nutzen Sie den {{ page.twifThread }} im Forum. Einsendeschluss
für das nächste TWIF ist **Donnerstag** 12:00 UTC.

Ein allgemeines Feedback kann ebenfalls über Mastodon abgegeben werden oder,
wenn Sie gerne an einem Live-Chat teilnehmen möchten, dann finden Sie uns in
**#fdroid** auf [Freenode](https://freenode.net), auf Matrix über {{
page.matrixRoom }} oder auf [Telegram]({{ page.telegramRoom }}). Alle diese
Orte werden miteinander verbunden, Sie haben also die Wahl. Sie können sich
uns auch im **[Forum]({{ page.forum }})** anschließen.
