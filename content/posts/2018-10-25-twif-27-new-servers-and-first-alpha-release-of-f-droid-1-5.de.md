---
layout: post
title: "TWIF 27: Neue Server und erstes Alpha-Release von F-Droid 1.5"
lang: de
edition: 27
author: "Coffee"
authorWebsite: "https://open.source.coffee"
fdroid: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #0d47a1; font-style: normal; font-weight: bold;">F-Droid</em>'
featuredv1: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.5ex; box-shadow: 0.1em 0.05em 0.1em rgba(0, 0, 0, 0.3); border-radius: 1em; color: black; background: linear-gradient(orange, yellow);">Featured</em>'
featured: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: orange; font-style: normal; font-weight: bold;">Featured</em>'
major: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #8ab000; font-style: normal; font-weight: bold;">Major</em>'
number_of_updated_apps: 32
mastodonAccount: "**[@fdroidorg@mastodon.technology](https://mastodon.technology/@fdroidorg)**"
twifTag: "**[#TWIF](https://mastodon.technology/tags/twif)**"
twifThread: "[TWIF submission thread](https://forum.f-droid.org/t/twif-submission-thread)"
matrixRoom: "[#fdroid:matrix.org](https://matrix.to/#/#fdroid:matrix.org)"
telegramRoom: "https://t.me/joinchat/AlRQekvjWDTuQrCgMYSNVA"
forum: "https://forum.f-droid.org"
---


Diese Woche In F-Droid {{ page.edition }}, Woche {{ page.date | date: "%V, %G" }} <a href="{{ site.baseurl }}/feed.xml"><img src="{{ site.baseurl }}/assets/Feed-icon-16x16.png" alt="Feed"></a>

In dieser Ausgabe: Zwei neue Hochleistungsserver, erstes Alpha-Release von F-Droid 1.5, Metadatenbereinigung, Einrichtung des automatischen Buildservers, portugiesischer Gerichtsentscheid gegen Google und Bestätigung für F-Droid, frei von Malware zu sein. Es gibt 4 neue und {{ page.number_of_updated_apps }} aktualisierte Apps.
<!--more-->

[F-Droid](https://f-droid.org/) ist ein [Repository](https://f-droid.org/de/packages/) verifizierter [Free & Open Source](https://de.wikipedia.org/wiki/Free/Libre_Open_Source_Software) Android Apps, ein [Client](https://f-droid.org/app/org.fdroid.fdroid), um darauf zuzugreifen, sowie ein ganzes "App Store Set", das alle notwendigen Werkzeuge zum Einrichten und Betreiben eines App Stores bietet. Es ist ein gemeinschaftlich geführtes freies Softwareprojekt, das von einer Vielzahl von Mitwirkenden weiterentwickelt wird. Das ist ihre Geschichte der letzten Woche.

#### Zwei neue Server aus dem GCC Compile Farm Project

Vielen Dank dem [GCC Compile Farm Project](https://cfarm.tetaneutral.net),
das zwei dedizierte Server für F-Droid eingerichtet hat, jeder mit 16 Cores,
145 GB RAM und 2.6 TB Platte. Diese sind eigentlich alte Facebook-Maschinen,
aber deren Spezifikationen sind immer noch einigermaßen beeindruckend! Und
sie gibt es _zusätzlich_ zum [F-Droid-Spiegelserver von Tetaneutral]({{
site.baseurl }}{% post_url
2018-10-12-twif-25-tor-browser-matrix-room-enhancements-and-mirror-beta-testing
%}#beta-mirror-up-for-testing).

Das _GCC Compile Farm Project_ ist ein Vermittler für Entwickler freier
Software, um Zugriff auf Ressourcen zu erlangen. Das Managementsystem für
dieses Projekt wird von [Tetaneutral](https://tetaneutral.net) gehostet und
die Maschinen selbst werden von verschiedenen Organisationen rund um den
Globus gehostet. Das eigentliche Hosting dieser beiden Maschinen erfolgt
durch das [Oregon State University Open Source Lab](https://osuosl.org)
(OSUOSL).

Für Leute, die mit ihnen arbeiten werden: Behalten Sie im Hinterkopf, dass
diese Maschinen wirkliche Hard Disks besitzen, die nicht in einer
RAID-Konfiguration laufen. Sie sollten nur entbehrliche Inhalte aufnehmen,
da die Hard Disks versagen können.

#### Erste Alpha-Version des F-Droid-Client 1.5 veröffentlicht

F-Droid-Client 1.5-alpha0 kam heraus mit einem neuen ROM-Feature. Es gibt
nun [einen Weg, F-Droid mit zusätzlichen Repositorys
vorzuinstallieren](https://gitlab.com/fdroid/fdroidclient/merge_requests/705).
Diese Repositorys können nur ergänzt werden und können sich nicht über das
Hauptrepository f-droid.org hinwegsetzen. Nachdem der Anwender frei darüber
entscheiden kann, es zu deaktivieren oder zu löschen, gilt dies nur für die
Ersteinrichtung.

So könnte zum Beispiel Lineage ein Image mit einer vorinstallierten
Standard-F-Droid-APK bereitstellen und es mit einem speziellen Repository
vorkonfigurieren. Ebenso könnte das Replicant, Fairphone usw. tun. Dieses
Feature wurde in Zusammenarbeit mit Emteria entworfen, die es größtenteils,
mit wesentlichen Beiträgen von F-Droid-Leuten, umgesetzt haben.

#### F-Droid-Metadatenbereinigung

**[@Izzy](https://forum.f-droid.org/u/izzy)** beschäftigt sich intensiv hinter den Kulissen mit den Drecksarbeiten, die keiner sonst machen will. In [dieser Runde](https://gitlab.com/fdroid/fdroiddata/issues/1414) entfernt er alle Website-Links, die lediglich auf den Quellcode verweisen.

Wenn Sie einem "Website"-Link auf einer Seite mit einer App-Beschreibung
folgen, sollte er zu einer wirklichen Website führen. Ansonsten sollte er
nicht vorhanden sein. Unsere Metadaten sind voll mit Fällen, in denen auf
den Quellcode verlinkt wird, entweder direkt durch Verdoppelung des
"Quelltext"-Links oder durch Verweis auf etwas sehr Ähnliches, zum Beispiel
die README-Datei. Diese Arbeiten werden das beheben und die Art, in der wir
auf etwas verlinken, vereinheitlichen, was für ein feineres Nutzererlebnis
sorgen wird.

#### Fortschritte bei der Konfiguration des automatischen Buildservers

Diese Woche erstellten wir das neue Projekt
[fdroid-bootstrap-buildserver](https://gitlab.com/fdroid/fdroid-bootstrap-buildserver)
in Gitlab. Dieses Projekt enthält das
[Ansible](https://en.wikipedia.org/wiki/Ansible_%28software%29)-Skript, das
für die Einrichtung und Konfiguration eines vollautomatischen Buildservers
verantwortlich ist. Es hat bereits erfolgreich zwei Buildserver
eingerichtet. Sobald das Skript alles erledigen kann, was kurz bevor steht,
wird die gesamte Buildserver-Einrichtung reproduzierbar und frei verfügbar
sein. Dies ist gewaltig für uns, insbesondere in Kombination mit den
o. g. Nachrichten.

#### Portugiesischer Gerichtsbeschluss gegen Google auf Antrag von Aptoide

Aptoide ist ein alternativer App-Store. Übrigens derjenige, aus dem die
F-Droid-App ursprünglich abgezweigt wurde. Anscheinend hat Google den
Aptoide-App-Store auf über 2,2 Millionen Geräten in den letzten 60 Tagen
ferngesteuert entfernt/ausgeblendet.

Glücklicherweise hat der Portugiesische Gerichtshof entschieden, dass Google
dies zu unterlassen hat. Wobei dies erst der Anfang ist, da Aptoide auch
Anzeige wegen der dadurch entstandenen Schäden erstatten wird. Lesen Sie
[hier](https://www.prnewswire.com/news-releases/aptoide-eu-national-court-rules-against-google-in-anti-trust-process-821883497.html)
die ganze Geschichte.

#### Unabhängige, wissenschaftliche Bestätigung, dass F-Droid frei von Malware ist

Wir haben eben erst davon erfahren, obwohl die Studie eigentlich im Jahr
2017 durchgeführt wurde. Wir sind nun im Besitz einer weiteren,
unabhängigen, wissenschaftlichen Bestätigung von 0 Malware in F-Droid. Laden
Sie sich die Studienergebnisse [hier
(PDF)](https://nsl.cs.waseda.ac.jp/wp-content/uploads/2018/04/submitted_wama2017.pdf)
herunter.

#### Neue Apps

* **[Hide "running in the background"
  Notification](https://f-droid.org/app/com.iboalali.sysnotifsnooze)**: Die
  lästige Benachrichtigung "läuft im Hintergrund" ausblenden.
* **[lWS](https://f-droid.org/app/net.basov.lws.fdroid)**: Leichtgewichtiger
  Webserver (lws).
* **[lWS QR](https://f-droid.org/app/net.basov.lws.qr.fdroid)**:
  QR-Codegenerator-Erweiterung für lWS (lws qr).
* **[Weather
  Forecast](https://f-droid.org/app/uk.org.boddie.android.weatherforecast)**:
  Wettervorhersagen von yr.no ansehen.

#### Aktualisierte Apps

Insgesamt wurden diese Woche **{{ page.number_of_updated_apps }}** Apps
aktualisiert. Hier die Höhepunkte:

* {{ page.major }} Weitere Updates für die Simple Mobile Tools. Dieses Mal
  wurden
  **[Calculator](https://f-droid.org/app/com.simplemobiletools.calculator)**,
  **[Music
  Player](https://f-droid.org/app/com.simplemobiletools.musicplayer)** und
  **[Notes](https://f-droid.org/app/com.simplemobiletools.notes)** auf die
  Hauptversion 5 aktualisiert, mit der wiederum die
  Android-Minimalanforderung von 4.1 auf 5.0 angehoben und der G+ Button
  durch Reddit ersetzt wurde.

* **[Periodical](https://f-droid.org/app/de.arnowelzel.android.periodical)**
  wurde von 1.16 auf 1.18 aktualisiert, bringt überarbeitete Übersetzungen
  aus Crowdin mit und führt Sprachcodes auf ihre Stammform zurück, um
  fehlende Übersetzungen zu vermeiden, wenn eine regionale Spracheinstellung
  verwendet wird.

* {{ page.major }}
  **[Zapp](https://f-droid.org/app/de.christinecoenen.code.zapp)**
  veröffentlichte Version 2.0.0, mit Wiedergabe von "Mediathek"-Videos im
  Hintergrund, automatischer Pause, wenn eine andere App Töne wiedergibt
  oder während eines Telefonanrufs, einem Sperrbildschirm-Widget, einer
  Fortschrittsanzeige, die nur angezeigt wird, wenn das Video wirklich
  stehenbleibt und einer von 4.4 auf 5.0 erhöhten
  Android-Minimalanforderung.

* **[AntennaPod](https://f-droid.org/app/de.danoeh.antennapod)** wurde von
  1.6.5 auf 1.7.0 aktualisiert, mit dem neuen ExoPlayer, einer Korrektur für
  Bluetooth Forward, einem neuen Design für Einstellungen + Suche,
  Benachrichtigungsverbesserungen, verschiedenen Bildschirmen für Feed-Infos
  und -Einstellungen, zufällig oder smart gemischter
  Warteschlangensortierung, einem wirklich schwarzen AMOLED-Thema,
  Verbesserungen am Feed-Parsing und einer Fehlerlösung für das Beenden
  durch Android Oreo.

* {{ page.major }}
  **[AFWall+](https://f-droid.org/app/dev.ukanth.ufirewall)**
  veröffentlichte die Version 3.0.0 mit besserer Unterstützung für Android
  Nougat, Oreo und Pie; einem Firewall-Schalter; adaptiven Icons;
  Benachrichtigungskanal; Kompatibilität mit Magisk 17.x; besserer
  Handhabung von Hintergrundprozessen; aktualisierten Bibliotheken,
  Übersetzungen und Fehlerkorrekturen. Die Android-Minimalanforderungen
  wurden von 4.0.3 auf 5.0 angehoben.

* **[SQRL - Main](https://f-droid.org/app/org.ea.sqrl)** aktualisiert auf
  Version 0.11.0, die nun mit der Bildschirmdrehung umgehen kann, eine
  manuelle Sprachauswahl anbietet und eine x=n Funktionalität umsetzt. Sie
  enthält drei neue Sprachen: Französisch, Japanisch und Spanisch.

* **[Your local
  weather](https://f-droid.org/app/org.thosp.yourlocalweather)** Update von
  4.2.6 auf 4.3.1, mit neu geschriebenen Hintergrunddiensten für schnellere
  und reibungslosere Aktualisierungen, Anzeige der Windrichtung in aktueller
  Wetterlage und -vorhersage und einer Option für ein Zeitformat.

#### Anregungen und Rückmeldung

Haben Sie wichtige Updates einer App, über die wir schreiben sollten? Senden
Sie Ihre Tipps über [Mastodon](https://joinmastodon.org) ein! Senden Sie sie
an {{ page.mastodonAccount }} und denken Sie an einen {{ page.twifTag
}}-Tag. Oder nutzen Sie den {{ page.twifThread }} im Forum. Einsendeschluss
für das nächste TWIF ist **Donnerstag** 12:00 UTC.

Ein allgemeines Feedback kann ebenfalls über Mastodon abgegeben werden oder,
wenn Sie gerne an einem Live-Chat teilnehmen möchten, dann finden Sie uns in
**#fdroid** auf [Freenode](https://freenode.net), auf Matrix über {{
page.matrixRoom }} oder auf [Telegram]({{ page.telegramRoom }}). Alle diese
Orte werden miteinander verbunden, Sie haben also die Wahl. Sie können sich
uns auch im **[Forum]({{ page.forum }})** anschließen.
