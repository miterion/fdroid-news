---
layout: post
title: "TWIF 24: Das Ende eines Protokolls, so wie wir es kennen"
lang: de
edition: 24
author: "Coffee"
authorWebsite: "https://open.source.coffee"
major: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #8ab000; font-style: normal; font-weight: bold;">Major</em>'
number_of_updated_apps: 25
---


Diese Woche In F-Droid {{ page.edition }}, Woche {{ page.date | date: "%V, %G" }} <a href="{{ site.baseurl }}/feed.xml"><img src="{{ site.baseurl }}/assets/Feed-icon-16x16.png" alt="Feed"></a>

Eine dünne Ausgabe diese Woche, da der Update-Prozess seit Montag feststeckt. Diese Woche erfahren wir von der Schließung von BotBot.me, 4 neuen Apps und {{ page.number_of_updated_apps }} Aktualisierungen, darunter ein wichtiges Update für SnoopSnitch.
<!--mehr-->

[F-Droid](https://f-droid.org/) ist ein [Repository](https://f-droid.org/de/packages/) verifizierter [Free & Open Source](https://de.wikipedia.org/wiki/Free/Libre_Open_Source_Software) Android Apps, ein [Client](https://f-droid.org/app/org.fdroid.fdroid), um darauf zuzugreifen, sowie ein ganzes "App Store Set", das alle notwendigen Werkzeuge zum Einrichten und Betreiben eines App Stores bietet. Es ist ein gemeinschaftlich geführtes freies Softwareprojekt, das von einer Vielzahl von Mitwirkenden weiterentwickelt wird. Das ist ihre Geschichte der letzten Woche.

#### BotBot.me schließt seine Pforten

Unter Berücksichtigung der neuen
[DSGVO](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)
und mangels Entwicklung seit 2015 wird der IRC-Ereignisprotokolldienst
[BotBot.me](https://botbot.me) nach 6 Dienstjahren [am 5. November 2018
geschlossen](https://lincolnloop.com/blog/saying-goodbye-botbotme/)
werden. BotBot.me protokolliert unsere öffentlichen IRC-Räume,
[#fdroid](https://botbot.me/freenode/fdroid/) und
[#fdroid-dev](https://botbot.me/freenode/fdroid-dev/). Wir überlegen im
Moment, was wir mit den alten Protokollen machen und wodurch wir ihn, wenn
überhaupt, ersetzen sollen.

#### Neue Apps

* **[Nomad](https://f-droid.org/app/com.dfa.hubzilla_android)** ist ein
  inoffizieller Client für das soziale Netzwerk Hubzilla
* **[X11-Basic](https://f-droid.org/app/net.sourceforge.x11basic)**:
  X11-Basic ist ein Dialekt der Programmiersprache BASIC mit grafischen
  Fähigkeiten. Sie besitzt einen reichhaltigen Befehlssatz, ist aber immer
  noch leicht zu erlernen.
* **[Disroot App](https://f-droid.org/app/org.disroot.disrootapp)**: Die
  Disroot-App ist Ihr Schweizer Armeemesser für die Disroot-Plattform
* **[AFReplacer](https://f-droid.org/app/ru.ifproject.android.afr)**:
  Amazfit Bip Watchfaces in Mi Fit austauschen

#### Aktualisierte Apps

Insgesamt wurden diese Woche **{{ page.number_of_updated_apps }}** Apps
aktualisiert. Hier die Höhepunkte:

* **[Apple
  Flinger](https://f-droid.org/app/com.gitlab.ardash.appleflinger.android)**
  ist ein durch Angry Birds inspiriertes Spiel. Version 1.5.0 fügt Erfolge
  hinzu und behebt einen Fehler bei den Spielergebnissen.

* **[WaveUp](https://f-droid.org/app/com.jarsilio.android.waveup)**: Telefon
  aufwecken - Bildschirm einschalten - durch Handbewegungen über dem
  Näherungssensor. Version 2.3.0 besitzt eine neue Liste "ausgeschlossener
  Apps".

* {{ page.major }}
  **[SnoopSnitch](https://f-droid.org/app/de.srlabs.snoopsnitch)** wurde von
  1.0.3 auf 2.0.7 aktualisiert. SnoopSnitch ist ein Werkzeug, um auf
  kompatiblen, gerooteten Telefonen die Mobilnetz-Sicherheit zu
  prüfen. Dieses Update führt die Funktion der Android-Patch-Analyse ein und
  reduziert den Akkuverbrauch. Es beinhaltet auch diverse
  UI/UX-Verbesserungen, Fehlerlösungen und Stabilitätsverbesserungen.

* **[FairEmail](https://f-droid.org/app/eu.faircode.email)** wurde von 0.69
  auf 0.71 aktualisiert, die als Alternative jetzt Plain Text versendet, hat
  aktualisierte Tools und Bibliotheken, auf den neusten Stand gebrachte
  Übersetzungen und verschiedene kleine Verbesserungen und Korrekturen.

* **[Mastalab](https://f-droid.org/app/fr.gouv.etalab.mastodon)**, der
  Mastodon-Client, stempelt auf F-Droid die 1.13.6. Unterhaltungen können
  jetzt aus entfernten Instanzen eröffnet werden und eine geeignete
  Medienvorschau wird in einem 2x2-Raster angezeigt. Probleme mit URLs,
  Filtern und Benachrichtigungen wurden bearbeitet und einige
  Absturzursachen behoben.

* **[Authorizer](https://f-droid.org/app/net.tjado.passwdsafe)**
  veröffentlicht ihre erste stabile Version, 0.3.0, nach mehr als einem Jahr
  Inaktivität. Authorizer verwandelt ein altes Smartphone in einen
  Hardware-Passwort-Manager, indem sie eine USB-Tastatur emuliert. Diese
  Version benötigt jetzt Android 5.0 (vorher 4.0), um zu laufen, und ergänzt
  ein OTP-Feature, eine Platzhalterfunktion, eine asymmetrisch
  verschlüsselte Sicherung, Datenschutzrichtlinien und verschiedene
  Fehlerkorrekturen sowie kleinere Änderungen.

#### Anregungen und Rückmeldung

Haben Sie wichtige Updates einer App, über die wir schreiben sollten? Senden
Sie Ihre Tipps über [Mastodon](https://joinmastodon.org) ein! Senden Sie sie
an
**[@fdroidorg@mastodon.technology](https://mastodon.technology/@fdroidorg)**
und denken Sie an einen
**[#TWIF](https://mastodon.technology/tags/twif)**-Tag. Oder nutzen Sie den
[TWIF Submission Thread](https://forum.f-droid.org/t/twif-submission-thread)
im Forum. Einsendeschluss für das nächste TWIF ist **Donnerstag** 12:00 UTC.

Ein allgemeines Feedback kann ebenfalls über Mastodon abgegeben werden oder,
wenn Sie gerne an einem Live-Chat teilnehmen möchten, dann finden Sie uns in
**#fdroid** auf [Freenode](https://freenode.net), auf Matrix über
[#freenode_#fdroid:matrix.org](https://matrix.to/#/#freenode_#fdroid:matrix.org)
oder auf [Telegram](https://t.me/joinchat/AlRQekvjWDTuQrCgMYSNVA). Alle
diese Orte werden miteinander verbunden, Sie haben also die Wahl. Sie können
sich uns auch im **[Forum](https://forum.f-droid.org/)** anschließen.
